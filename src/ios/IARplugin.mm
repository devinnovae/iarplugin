//
//  IARplugin.m
//  PluginBioIberica
//
//  Created by Innovae on 3/7/15.
//
//

#import "IARplugin.h"
#import "MainViewController.h"
#import "INNVisorRealidadAumentadaViewController.h"
#import "VideoPlaybackViewController.h"


@implementation IARplugin


- (void)launch:(CDVInvokedUrlCommand *)command {
    
    NSLog(@"Se ha ejecutado el plugin de Realidad Aumentada");
    
    CDVPluginResult *resultadoEjecucionPlugin = nil;
    
    /*
     * Es el único controlador de vista del aplicación ionic
     * */
    MainViewController *controladorVistaWeb =  (MainViewController *) [self controladorVistaActual];
    VideoPlaybackViewController *controladorVideo = [[VideoPlaybackViewController alloc] init];
    INNVisorRealidadAumentadaViewController *controladorVistaRA = [[INNVisorRealidadAumentadaViewController alloc] initWithRootViewController:controladorVideo];
    [controladorVistaWeb presentViewController:controladorVistaRA animated:YES completion:^{
        NSLog(@"Instanciando el controlador de Realidad Aumentada");
    }];

    if (controladorVistaRA != nil) {
        resultadoEjecucionPlugin = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        resultadoEjecucionPlugin = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [[self commandDelegate] sendPluginResult:resultadoEjecucionPlugin callbackId:[command callbackId]];
}


- (UIViewController *)controladorVistaActual {
    
    UIViewController *controladorVistaActual = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    return controladorVistaActual;
}

@end
