//
//  IARplugin.h
//  PluginBioIberica
//
//  Created by Innovae on 3/7/15.
//
//

#import <Cordova/CDVPlugin.h>

@interface IARplugin : CDVPlugin

- (void)launch:(CDVInvokedUrlCommand *)command;

@end
